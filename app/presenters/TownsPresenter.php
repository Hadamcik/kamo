<?php
/**
 * Towns presenter
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;

use Nette;


/**
 * Class TownsPresenter
 * @package App\Presenters
 */
class TownsPresenter extends Nette\Application\UI\Presenter
{
    use TranslatorTrait;
    use AuthorizatorTrait;
    use MainMenuTrait;

    /** @var int */
    private $id;

    /** @var \App\Model\TownsRepository @inject */
    public $towns;

    /** @var \App\Model\EventsRepository @inject */
    public $events;

    /** @var \App\Model\UsersTownsRepository @inject */
    public $usersTowns;

    /** @var \App\Components\IEventTileFactory @inject */
    public $eventTileFactory;

    /**
     * Process towns default
     * @param int $id
     */
    public function actionDefault($id) {
        $this->id = $id;
        $town = $this->towns->get($id);
        if($town == NULL) {
            $this->flashMessage('towns.notfound', 'error');
            $this->redirect('Homepage:default');
        }
        $this->template->town = $town;
        $this->template->favouritedCity = $this->usersTowns->isFavourited($this->user->getId(), $id);
        $this->template->events = $this->events->getByTown($id, 20);
    }

    /**
     * Creates event tile component
     * @return \App\Components\EventTile
     */
    protected function createComponentEventTile()
    {
        return $this->eventTileFactory->create();
    }

    public function handleFavourite($favourite) {
        if($this->user->isLoggedIn()) {
            $this->usersTowns->favourite($this->user->getId(), $this->id, $favourite);
            $this->template->favouritedCity = $favourite;
            $this->redrawControl('favouriteCity');
        }
    }
}
