<?php
/**
 * Sign presenter
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Class SignPresenter
 * @package App\Presenters
 */
class SignPresenter extends Nette\Application\UI\Presenter
{
    use TranslatorTrait;
    use AuthorizatorTrait;
    use MainMenuTrait;

    /** @var \App\Components\ISignInFormFactory @inject */
    public $signInFormFactory;

    /** @var \App\Components\ISignUpFormFactory @inject */
    public $signUpFormFactory;

    /** @var \App\Model\UsersRepository @inject */
    public $users;

    /** @var \App\Model\RolesRepository @inject */
    public $roles;

    /**
     * Creates sign in form component
     * @return \App\Components\SignInForm
     */
    protected function createComponentSignInForm()
    {
        return $this->signInFormFactory->create();
    }

    /**
     * Creates sign up form component
     * @return \App\Components\SignUpForm
     */
    protected function createComponentSignUpForm()
    {
        $signUpForm = $this->signUpFormFactory->create();
        $signUpForm->setUsersRepository($this->users);
        $signUpForm->setRolesRepository($this->roles);
        return $signUpForm;
    }

    /**
     * Proceed sign in
     */
    public function actionIn()
    {
        $this->template->title = $this->translator->translate('sign.in.title');
    }

    /**
     * Proceed sign up
     */
    public function actionUp()
    {
        $this->template->title = $this->translator->translate('sign.up.title');
    }

    /**
     * Proceed sign out
     */
    public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('sign.out.success');
		$this->redirect('Homepage:default');
	}

}
