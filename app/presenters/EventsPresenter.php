<?php
/**
 * Events presenter
 */

namespace App\Presenters;

use Nette;


/**
 * Class EventsPresenter
 * @package App\Presenters
 */
class EventsPresenter extends Nette\Application\UI\Presenter
{
    use TranslatorTrait;
    use AuthorizatorTrait;
    use MainMenuTrait;

    /** @var \App\Model\EventsRepository @inject */
    public $events;

    /** @var \App\Components\IEventAddEditFormFactory @inject */
    public $eventAddEditFormFactory;

    /**
     * Proceed events default
     * @param int $id
     */
    public function actionDefault($id) {
        $event = $this->events->get($id);
        if($event == FALSE) {
            $this->flashMessage('events.default.notFound', 'error');
            $this->redirect('Homepage:default');
        }
        $this->template->event = $event;
    }

    /**
     * Creates event add/edit form component
     * @return \App\Components\Calendar
     */
    protected function createComponentEventAddEditForm()
    {
        return $this->eventAddEditFormFactory->create();
    }
}
