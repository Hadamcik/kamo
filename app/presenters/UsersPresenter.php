<?php
/**
 * Users presenter
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;

use Nette;


/**
 * Class UsersPresenter
 * @package App\Presenters
 */
class UsersPresenter extends Nette\Application\UI\Presenter
{
    use TranslatorTrait;
    use AuthorizatorTrait;
    use MainMenuTrait;

    /** @var \App\Model\UsersRepository @inject */
    public $users;

    /** @var \App\Components\IUserSettingsFormFactory @inject */
    public $userSettingsFormFactory;

    /**
     * Creates user settings form component
     * @return \App\Components\UserSettingsForm
     */
    protected function createComponentUserSettingsForm()
    {
        $form = $this->userSettingsFormFactory->create();
        $form->setUsers($this->users);
        return $form;
    }
}
