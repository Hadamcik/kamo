<?php
/**
 * Homepage presenter
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;

use Nette;

/**
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends Nette\Application\UI\Presenter
{
    use TranslatorTrait;
    use AuthorizatorTrait;
    use MainMenuTrait;

    /** @var int */
    private $districtId;

    /** @var \App\Components\INavigateFilterFactory @inject */
    public $navigateFilterFactory;

    /** @var \App\Components\ICalendarFactory @inject */
    public $calendarFactory;

    /** @var \App\Model\DistrictsRepository @inject */
    public $districts;

    /** @var \App\Model\TownsRepository @inject */
    public $towns;

    /**
     * Process homepage default
     */
    public function actionDefault($id = NULL) {
        $this->districtId = $id;
    }

    /**
     * Creates navigate filter component
     * @return \App\Components\NavigateFilter
     */
    protected function createComponentNavigateFilter()
    {
        if($this->districtId == NULL) {
            $items = $this->districts->getList();
            $type = 'districts';
        }
        else {
            $items = $this->towns->getList($this->districtId);
            $type = 'towns';
        }
        $navigate = $this->navigateFilterFactory->create();
        $navigate->setItems($items);
        $navigate->setType($type);
        return $navigate;
    }

    /**
     * Creates calendar component
     * @return \App\Components\Calendar
     */
    protected function createComponentCalendar()
    {
        return $this->calendarFactory->create();
    }
}
