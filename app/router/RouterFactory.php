<?php
/**
 * Router factory
 */

namespace App;

use Nette,
	Nette\Application\Routers\RouteList,
	Nette\Application\Routers\Route;


/**
 * Class RouterFactory
 * @package App
 */
class RouterFactory
{

	/**
     * Creates router
	 * @return \Nette\Application\IRouter
	 */
	public function createRouter()
	{
		$router = new RouteList();
        $router[] = new Route('[<locale=cs cs|en>/]charity/detail/<slug>', 'Charities:detail');
		$router[] = new Route('[<locale=cs cs|en>/]<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}
