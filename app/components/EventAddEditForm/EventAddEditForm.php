<?php
/**
 * Event add/edit form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class EventAddEditForm
 * @package App\Components
 */
class EventAddEditForm extends UI\Control
{
    use TemplateTrait;
}

/**
 * Interface IEventAddEditFormFactory
 * @package App\Components
 */
interface IEventAddEditFormFactory
{
    /**
     * Creates event add/edit form component
     * @return \App\Components\EventAddEditForm
     */
    function create();
}
