<?php
/**
 * EventTile component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class EventTile
 * @package App\Components
 */
class EventTile extends UI\Control
{
    use TemplateTrait;

    public function render($event) {
        $this->template->event = $event;
        $this->template->render();
    }
}

/**
 * Interface IEventTileFactory
 * @package App\Components
 */
interface IEventTileFactory
{
    /**
     * Creates event title component
     * @return \App\Components\EventTile
     */
    function create();
}
