<?php
/**
 * Calendar component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class Calendar
 * @package App\Components
 */
class Calendar extends UI\Control
{
    use TemplateTrait;
}

/**
 * Interface ICalendarFactory
 * @package App\Components
 */
interface ICalendarFactory
{
    /**
     * Creates calendar component
     * @return \App\Components\Calendar
     */
    function create();
}
