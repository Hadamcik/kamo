<?php
/**
 * User settings form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class UserSettingsForm
 * @package App\Components
 */
class UserSettingsForm extends UI\Control
{
    use TemplateTrait;

    /** @var \App\Model\UsersRepository */
    private $users;

    /**
     * Declares settings password form
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentPasswordForm() {
        $form = new UI\Form();

        $form->addPassword('oldpassword', 'users.settings.form.oldpassword')
            ->setRequired('users.settings.form.fillOldPassword');
        $form->addPassword('password', 'users.settings.form.password')
            ->setRequired('users.settings.form.fillPassword');
        $form->addPassword('rePassword', 'users.settings.form.rePassword')
            ->addRule(UI\Form::EQUAL, 'users.settings.form.passwordsNotMatch', $form['password'])
            ->setRequired();

        $form->addSubmit('send', 'users.settings.form.submitPassword');

        $form->onSuccess[] = $this->processPasswordForm;

        $form->setTranslator($this->presenter->translator);
        return $form;
    }

    /**
     * Process password form submit
     * @param \Nette\Application\UI\Form $form
     */
    public function processPasswordForm(\Nette\Application\UI\Form $form) {
        $values = $form->getValues();
        $correctPassword = $this->users->checkPassword($this->presenter->user->getId(), $values->oldpassword);
        if($correctPassword) {
            $updated = $this->users->changePassword($this->presenter->user->getId(), $values->password);
            if($updated != 0) {
                $this->presenter->flashMessage('users.settings.form.passwordSuccess', 'success');
            }
            else {
                $this->presener->flashMessage('users.settings.form.passwordError', 'error');
            }
        }
        else {
            $this->presenter->flashMessage('users.settings.form.incorectOldPassword', 'error');
        }
        $this->presenter->redirect('this');
    }

    /**
     * Declares settings others form
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentOthersForm() {
        $form = new UI\Form();
        $form->addText('firstname', 'users.settings.form.firstname')
            ->setDefaultValue($this->presenter->user->getIdentity()->firstname)
            ->setRequired('users.settings.form.fillFirstname');
        $form->addText('lastname', 'users.settings.form.lastname')
            ->setDefaultValue($this->presenter->user->getIdentity()->lastname)
            ->setRequired('users.settings.form.fillLastname');

        $form->addSubmit('send', 'users.settings.form.submitOthers');

        $form->onSuccess[] = $this->processOthersForm;

        $form->setTranslator($this->presenter->translator);
        return $form;
    }

    /**
     * Process others form submit
     * @param \Nette\Application\UI\Form $form
     */
    public function processOthersForm(\Nette\Application\UI\Form $form) {
        $values = $form->getValues();

        $updated = $this->users->changeOthers($this->presenter->user->getId(), $values->firstname, $values->lastname);
        if($updated != 0) {
            $this->presenter->user->getIdentity()->firstname = $values->firstname;
            $this->presenter->user->getIdentity()->lastname = $values->lastname;
            $this->presenter->flashMessage('users.settings.form.othersSuccess', 'success');
        }
        else {
            $this->presenter->flashMessage('users.settings.form.othersSuccess', 'error');
        }
        $this->presenter->redirect('this');
    }

    /**
     * @param \App\Model\UsersRepository $users
     */
    public function setUsers(\App\Model\UsersRepository $users) {
        $this->users = $users;
    }
}

/**
 * Interface IUserSettingsFormFactory
 * @package App\Components
 */
interface IUserSettingsFormFactory
{
    /**
     * Creates user settings form component
     * @return \App\Components\UserSettingsForm
     */
    function create();
}
