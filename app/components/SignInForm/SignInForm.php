<?php
/**
 * Sign in form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class SignInForm
 * @package App\Components
 */
class SignInForm extends UI\Control 
{
    use TemplateTrait;

    /**
     * Declares sign in form
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentForm() {
        $form = new UI\Form();

        $form->addText('email', 'sign.in.form.email')
            ->setRequired('sign.in.form.fillEmail');

        $form->addPassword('password', 'sign.in.form.password')
            ->setRequired('sign.in.form.fillPassword');

        $form->addCheckbox('remember', 'sign.in.form.keepMe');

        $form->addSubmit('send', 'sign.in.form.submit');

        // call method signInFormSucceeded() on success
        $form->onSuccess[] = $this->processForm;

        $form->setTranslator($this->presenter->translator);
        return $form;
    }

    /**
     * Process sign in form
     * @param \Nette\Application\UI\Form $form
     */
    public function processForm(UI\Form $form)
    {
        $values = $form->getValues();

        if ($values->remember) {
            $this->presenter->getUser()->setExpiration('14 days', FALSE);
        } else {
            $this->presenter->getUser()->setExpiration('20 minutes', TRUE);
        }

        try {
            $this->presenter->getUser()->login($values->email, $values->password);
            $this->flashMessage('sign.in.success', 'success');
            $this->presenter->redirect('Homepage:');

        } catch (\Nette\Security\AuthenticationException $e) {
            $form->addError($this->presenter->translator->translate($e->getMessage()));
        }
    }
    
}

/**
 * Interface ISignInFormFactory
 * @package App\Components
 */
interface ISignInFormFactory 
{
    /**
     * Creates sign in form component
     * @return \App\Components\SignInForm
     */
    function create();
}
