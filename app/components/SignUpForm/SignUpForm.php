<?php
/**
 * SignUpForm component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;
use \App\Model\UsersRepository;
use \App\Model\RolesRepository;

/**
 * Class SignUpForm
 * @package App\Components
 */
class SignUpForm extends UI\Control 
{
    use TemplateTrait;

    /** @var \App\Model\UsersRepository */
    public $users;

    /** @var \App\Model\RolesRepository */
    public $roles;


    /**
     * Declares sign up form
     * @return \Nette\Application\UI\Form
     */
    protected function createComponentForm() {
        $form = new UI\Form();

        $form->addText('email', 'sign.up.form.email')
            ->addRule(UI\Form::EMAIL, 'sign.up.form.invalidEmail')
            ->addRule(UI\Form::MAX_LENGTH, $this->presenter->translator->translate('sign.up.form.maxEmailLength', NULL, array('length' => 256)), 256)
            ->setRequired('sign.up.form.fillEmail');
        $form->addPassword('password', 'sign.up.form.password')
            ->setRequired('sign.up.form.fillPassword');
        $form->addPassword('rePassword', 'sign.up.form.rePassword')
            ->addRule(UI\Form::EQUAL, 'sign.up.form.passwordsNotMatch', $form['password'])
            ->setRequired();
        $form->addText('firstname', 'sign.up.form.firstname')
            ->setRequired('sign.up.form.fillFirstname');
        $form->addText('lastname', 'sign.up.form.lastname')
            ->setRequired('sign.up.form.fillLastname');

        $form->addSubmit('send', 'sign.up.form.submit');

        $form->onSuccess[] = $this->processForm;

        $form->setTranslator($this->presenter->translator);
        return $form;
    }

    /**
     * Process form submit
     * @param \Nette\Application\UI\Form $form
     */
    public function processForm(\Nette\Application\UI\Form $form) {
        $values = $form->getValues();

        $existUser = $this->users->getByEmail($values->email);
        if($existUser !== FALSE) {
            if($existUser->password === Passwords::hash(Authenticator::removeCapsLock($values->password))) {
                $this->presenter->getUser()->login($values->email, $values->password);
                $this->presenter->redirect('Homepage:');
            }
            else {
                $form->addError($this->presenter->translator->translate('sign.up.emailExists'));
            }
        }
        else {
            $user = $this->users->add($values->email, $values->password, $values->firstname, $values->lastname, $this->roles->getByName('User')->id_role);
            if($user !== FALSE) {
                $this->presenter->flashMessage('sign.up.importSuccess');
                $this->presenter->redirect('Homepage:');
            }
            else {
                $form->addError($this->presenter->translator->translate('sign.up.importError'));
            }
        }
    }

    /**
     * @param \App\Model\UsersRepository $users
     */
    public function setUsersRepository(UsersRepository $users) {
        $this->users = $users;
    }

    /**
     * @param \App\Model\RolesRepository $roles
     */
    public function setRolesRepository(RolesRepository $roles) {
        $this->roles = $roles;
    }
    
}

/**
 * Interface ISignUpFormFactory
 * @package App\Components
 */
interface ISignUpFormFactory 
{
    /**
     * Creates component
     * @return \App\Components\SignUpForm
     */
    function create();
}
