<?php
/**
 * Main menu component
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class MainMenu
 * @package App\Components
 */
class MainMenu extends UI\Control 
{
    use TemplateTrait;
}

/**
 * Interface IMainMenuFactory
 * @package App\Components
 */
interface IMainMenuFactory 
{
    /**
     * Creates main menu component
     * @return \App\Components\MainMenu
     */
    function create();
}
