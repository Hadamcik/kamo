<?php
/**
 * City add/edit form component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class CityAddEditForm
 * @package App\Components
 */
class CityAddEditForm extends UI\Control
{
    use TemplateTrait;
}

/**
 * Interface ICityAddEditFormFactory
 * @package App\Components
 */
interface ICityAddEditFormFactory
{
    /**
     * Creates city add/edit form component
     * @return \App\Components\CityAddEditForm
     */
    function create();
}
