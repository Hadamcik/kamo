<?php
/**
 * Navigate filter component
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Components;

use Nette\Application\UI;

/**
 * Class NavigateFilter
 * @package App\Components
 */
class NavigateFilter extends UI\Control
{
    use TemplateTrait;

    /** @var array */
    private $items;

    /** @var string */
    private  $type;

    /**
     * Setter for items
     * @param $items
     */
    public function setItems($items) {
        $this->items = $items;
    }

    /**
     * Setter for type
     * @param $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * Renders component
     */
    public function render()
    {
        $this->template->items = $this->items;
        $this->template->type = $this->type;
        $this->template->render();
    }
}

/**
 * Interface INavigateFilterFactory
 * @package App\Components
 */
interface INavigateFilterFactory
{
    /**
     * Creates navigate filter component
     * @return \App\Components\NavigateFilter
     */
    function create();
}
