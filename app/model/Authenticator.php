<?php
/**
 * Authenticator
 */

namespace App\Model;

use Nette,
    Nette\Utils\Strings,
    Nette\Security\Passwords;


/**
 * Class Authenticator
 * @package App\Model
 */
class Authenticator extends Nette\Object implements Nette\Security\IAuthenticator
{

    /**
     * Contains database context connection
     * @var Nette\Database\Context
     */
    private $context;

    /**
     * Construct Authenticator class
     * @param Nette\Database\Context $context
     */
    public function __construct(Nette\Database\Context $context)
    {
        $this->context = $context;
    }


    /**
     * Performs an authentication.
     * @param array
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($email, $password) = $credentials;
        $password = self::removeCapsLock($password);

        $row = $this->context->table('user')->where('email', $email)->fetch();

        if (!$row) {
            throw new Nette\Security\AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);

        } elseif (!Passwords::verify($password, $row['password'])) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        } elseif (Passwords::needsRehash($row['password'])) {
            $row->update(array(
                'password' => Passwords::hash($password),
            ));
        }

        $arr = $row->toArray();
        unset($arr['password']);
        return new Nette\Security\Identity($arr['id_user'], $row->role->name, $arr);
    }

    /**
     * Fixes caps lock accidentally turned on.
     * @param string
     * @return string
     */
    public static function removeCapsLock($password)
    {
        return $password === Strings::upper($password)
            ? Strings::lower($password)
            : $password;
    }

}