<?php
/**
 * Comments repository
 *
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

/**
 * Class CommentsRepository
 * @package App\Model
 */
class CommentsRepository extends Repository
{
    /** Add comment to database
     * @param int $userId
     * @param int $eventId
     * @param string $text
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($userId, $eventId, $text) {

    }

    /**
     * Returns comments of event
     * @param int $eventId
     * @return \Nette\Database\Table\Selection
     */
    public function getByEvent($eventId) {

    }

    /**
     * Hide or show comment
     * @param int $id
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function toggle($id) {

    }
}
