<?php
/**
 * Roles repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

/**
 * Class RolesRepository
 * @package App\Model
 */
class RolesRepository extends Repository
{

    /**
     * Returns role ID by its name
     * @param string $name
     * $return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function getByName($name) {
        return $this->getOne(array(
            'name' => $name
        ));
    }
}
