<?php
/**
 * Users repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
namespace App\Model;
use Nette\Security\Passwords;

/**
 * Class UsersRepository
 * @package App\Model
 */
class UsersRepository extends Repository
{

    /**
     * Add new user to database
     * @param string $email
     * @param string $password
     * @param string $firstname
     * @param string $lastname
     * @param int $role
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($email, $password, $firstname, $lastname, $role)
    {
        return $this->getTable()->insert(array(
            'email' => $email,
            'password' => Passwords::hash(Authenticator::removeCapsLock($password)),
            'firstname' => $firstname,
            'lastname' => $lastname,
            'id_role' => $role
        ));
    }

    /**
     * Returns user by email
     * @param string $email
     * @return FALSE|\Nette\Database\Table\ActiveRow
     */
    public function getByEmail($email) {
        return $this->getOne(array(
            'email' => $email
        ));
    }

    /**
     * @param int $userId
     * @param string $password
     * @return int
     */
    public function changePassword($userId, $password) {
        return $this->getAll(array(
            'id_user'> $userId
        ))->update(array(
            'password' => Passwords::hash(Authenticator::removeCapsLock($password))
          ));
    }

    /**
     * @param int $userId
     * @param string $firstname
     * @param string $lastname
     * @return int
     */
    public function changeOthers($userId, $firstname, $lastname) {
        return $this->getAll(array(
            'id_user'> $userId
        ))->update(array(
                'firstname' => $firstname,
                'lastname' => $lastname
            ));
    }

    public function checkPassword($userId, $password) {
        $user = $this->getOne(array(
            'id_user' => $userId
        ));
        return Passwords::verify($password, $user['password']);
    }
}
