<?php
/**
 * Events repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;
use Nette\Utils\DateTime;

/**
 * Class EventsRepository
 * @package App\Model
 */
class EventsRepository extends Repository
{
    /**
     * Add event to database
     * @param int $town
     * @param string $name
     * @param DateTime $begin
     * @param DateTime $end
     * @param int $price
     * @param string $location
     * @param string $description
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($town, $name, DateTime $begin, DateTime $end, $price, $location, $description) {

    }

    /**
     * Modify event in database
     * @param int $id
     * @param int $town
     * @param string $name
     * @param DateTime $begin
     * @param DateTime $end
     * @param int $price
     * @param string $location
     * @param string $description
     * @return int
     */
    public function edit($id, $town, $name, DateTime $begin, DateTime $end, $price, $location, $description) {

    }

    /**
     * Returns events in district for given interval
     * @param int $district
     * @param null|\Nette\Utils\DateTime $from
     * @param null|\Nette\Utils\DateTime $to
     * @return \Nette\Database\Table\Selection
     */
    public function getDistrictInterval($district, $from = NULL, $to = NULL) {

    }

    /**
     * Returns events in town for given interval
     * @param int $town
     * @param null|\Nette\Utils\DateTime $from
     * @param null|\Nette\Utils\DateTime $to
     * @return \Nette\Database\Table\Selection
     */
    public function getTownInterval($town, $from = NULL, $to = NULL) {

    }

    public function getByTown($townId, $limit = NULL) {
         $events = $this->getAll(array(
             'id_town' => $townId
         ))->order('begin ASC');
        if($limit != NULL) {
            $events->limit($limit);
        }
        return $events;
    }
}
