<?php
/**
 * UsersTowns repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */
namespace App\Model;

/**
 * Class UsersTownsRepository
 * @package App\Model
 */
class UsersTownsRepository extends Repository
{
    /**
     * Contains entity name
     * @var string
     */
    protected $table = 'users_towns';

    /**
     * Add user's favourite town to database
     * @param int $userId
     * @param int $townId
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($userId, $townId) {

    }

    /**
     * Removed user's favourite town from database
     * @param int $userId
     * @param int $townId
     * @return int
     */
    public function delete($userId, $townId) {

    }

    /**
     * Returns user's favourites towns
     * @param int $userId
     * @return \Nette\Database\Table\Selection
     */
    public function getByUser($userId) {

    }

    /**
     * @param int $userId
     * @param int $townId
     * @return bool
     */
    public function isFavourited($userId, $townId) {
        $favourite = $this->getOne(array(
            'id_user' => $userId,
            'id_town' => $townId
        ));
        return ($favourite != FALSE);
    }

    /**
     * @param int $userId
     * @param int $townId
     * @param bool $favourite
     */
    public function favourite($userId, $townId, $favourite) {
        if($favourite == TRUE) {
            $this->getAll()->insert(array(
                'id_user' => $userId,
                'id_town' => $townId
            ));
        }
        else {
            $this->getAll(array(
                'id_user' => $userId,
                'id_town' => $townId
            ))->delete();
        }
    }
}
