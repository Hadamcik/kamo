<?php
/**
 * UsersEvents repository
 */

namespace App\Model;

/**
 * Class UsersEventsRepository
 * @package App\Model
 */
class UsersEventsRepository extends Repository
{
    /**
     * Contains entity name
     * @var string
     */
    protected $table = 'users_events';

    /**
     * Add user's participate in event to database
     * @param int $userId
     * @param int $eventId
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($userId, $eventId) {

    }

    /**
     * Modify user's participate in event in database
     * @param int $userId
     * @param int $eventId
     * @return int
     */
    public function delete($userId, $eventId) {

    }

    /**
     * Returns if user is participate in event
     * @param int $userId
     * @param int $eventId
     * @return bool
     */
    public function exists($userId, $eventId) {

    }

    /**
     * Returns events where user is participate
     * @param int $userId
     * @return \Nette\Database\Table\Selection
     */
    public function getByUser($userId) {

    }

    /**
     * Returns users which participate in event
     * @param int $eventId
     * @return \Nette\Database\Table\Selection
     */
    public function getByEvent($eventId) {

    }
}
