<?php
/**
 * Towns repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

/**
 * Class TownsRepository
 * @package App\Model
 */
class TownsRepository extends Repository
{

    /**
     * Add town to database
     * @param int $districtId
     * @param string $name
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    public function add($districtId, $name) {

    }

    /**
     * Modify town in database
     * @param int $id
     * @param string $name
     * @return int
     */
    public function edit($id, $name) {

    }

    /**
     * Returns list of towns
     * @param int $districtId NULL
     * @return array
     */
    public function getList($districtId = NULL) {
        if($districtId == NULL) {
            return $this->getAll()->fetchPairs('id_town', 'name');
        }
        else {
            return $this->getAll(array(
                'id_district' => $districtId
            ))->fetchPairs('id_town', 'name');
        }
    }
}
