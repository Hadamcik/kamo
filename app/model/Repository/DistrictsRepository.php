<?php
/**
 * Districts repository
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

/**
 * Class DistrictsRepository
 * @package App\Model
 */
class DistrictsRepository extends Repository
{
    /**
     * Returns list of districts
     * @return array
     */
    public function getList() {
        return $this->getAll()->fetchPairs('id_district', 'name');
    }
}
