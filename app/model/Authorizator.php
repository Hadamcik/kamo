<?php
/**
 * Authorizator
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

use Nette;

/**
 * Class Authorizator
 * @package App\Model
 */
class Authorizator extends Nette\Object implements Nette\Security\IAuthorizator
{

    /**
     * Contains authorizator
     * @var \Nette\Security\Permission
     */
    private $acl;

    /**
     * Constructs Authorizator class
     */
    public function __construct() {
        $this->acl = new Nette\Security\Permission();
        $this->setAuthorizator();
    }

    /**
     * Returns if user is allowed to proceed action
     * @param $role
     * @param $resource
     * @param null $privilege
     * @return bool
     */
    function isAllowed($role, $resource, $privilege = NULL)
    {
        return $this->acl->isAllowed($role, $resource, $privilege);
    }

    /**
     * Configure Authorizator
     */
    private function setAuthorizator() {
        $this->acl->addRole('guest');

        $this->acl->addResource('admin');

        $this->acl->allow('guest', 'admin');
    }

}