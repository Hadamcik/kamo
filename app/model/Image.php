<?php
/**
 * Image support functions
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Model;

use Nette;

/**
 * Class Image
 * @package App\Model
 */
class Image extends Nette\Object
{
    /** @var int */
    const THUMBNAIL = 0;

    /**
     * Config array for Image class
     * @var array
     */
    private $config;

    /**
     * Constructs Image class
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * Saves image
     * @param Nette\Image $image
     * @param string $path
     * @param string $name
     * @param int $width
     * @param int $height
     * @return string
     */
    public function save(Nette\Image $image, $path, $name, $width = 300, $height = 400)
    {
        if(!file_exists($path)) {
            mkdir($path);
        }
        $path .= DIRECTORY_SEPARATOR;
        if(file_exists($path . $name . '.png')) {
            $i = 0;
            while(file_exists($path . $name. '-' . ++$i . '.png'));
            $name = $name. '-' . $i . '.png';
        }
        else {
            $name = $name . '.png';
        }
        $image->resize($width, $height);
        $image->sharpen();
        $image->save($path . $name, 85, Nette\Image::PNG);
        return $name;
    }

    /**
     * Saves image thumbnail
     * @param Nette\Image $image
     * @param string $name
     * @param int|null $type
     * @return string
     */
    public function saveThumbnail(Nette\Image $image, $name, $type = NULL)
    {
        list($width, $height) = $this->getThumbnailSizes($type);
        $path = $this->getThumbnailPath($type);
        return $this->save($image, $path . $name, $name, $width, $height);
    }

    /**
     * Returns thumbnail path
     * @param string $name
     * @param null $type
     * @return string
     */
    public function getThumbnail($name, $type = NULL)
    {
        $path = $this->getThumbnailPath($type);
        if(file_exists($path . $name)) {
            $path .= $name. DIRECTORY_SEPARATOR;
            $i = 0;
            while(file_exists($path . $name . '-' . ++$i . '.png'));
            if($i !== 1) {
                return $path . $name . '-' . --$i . '.png';
            }
            return $path . $name . '.png';
        }
        else {
            list($width, $height) = $this->getThumbnailSizes($type);
            $path = $this->config['placeholder'] . $width . 'x' . $height . '.png';
            return $path;
        }
    }
} 