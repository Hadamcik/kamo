<?php
/**
 * Main menu trait
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;

/**
 * Class MainMenuTrait
 * @package App\Presenters
 */
trait MainMenuTrait 
{
    /** @var \App\Components\IMainMenuFactory @inject */
    public $mainMenuFactory;

    /**
     * Creates main menu component
     * @return \App\Components\MainMenu
     */
    protected function createComponentMainMenu(){
        return $this->mainMenuFactory->create();
    }
} 