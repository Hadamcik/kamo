<?php
/**
 * Authorizator trait
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;
use App\Model\Authorizator;


/**
 * Class AuthorizatorTrait
 * @package App\Presenters
 */
trait AuthorizatorTrait
{
    /**
     * Starts up authorizator trait
     */
    public function startup() {
        parent::startup();
        $this->user->setAuthorizator(new Authorizator());
    }

    /**
     * Deny user access
     * @param string $message
     * @param string $redirect
     */
    public function deny($message = 'You have no access to this section', $redirect = 'Homepage:default') {
        $this->flashMessage($message, 'error');
        $this->redirect($redirect);
    }
} 