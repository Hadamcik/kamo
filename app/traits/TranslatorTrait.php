<?php
/**
 * Translator trait
 * @author Jakub Hadamčík <jakub@hadamcik.cz>
 */

namespace App\Presenters;

/**
 * Class TranslatorTrait
 * @package App\Presenters
 */
trait TranslatorTrait
{
    /**
     * Contains locale
     * @persistent
     */
    public $locale;

    /** @var \Kdyby\Translation\Translator @inject */
    public $translator;

    /**
     * Creates template with translator
     * @param string
     * @return \Nette\Templating\ITemplate
     */
    protected function createTemplate($class = NULL) {
        $template = parent::createTemplate($class);
        $template->getLatte()->addFilter(NULL, callback($this->translator->createTemplateHelpers(), 'loader'));

        return $template;
    }
} 