Installing
----------
I suppose that you have web server and MySQL server. If you don't, install it.

1. Install Git. I suggest GitHub app (see https://windows.github.com/)
2. Open GitBash, navigate to your localhost working directory and type git clone https://<YOUR_BITBUCKET_USERNAME>@bitbucket.org/DreiserCZ/kamo.git
3. Hit enter and type your bitbucket password
4. Install Composer: (see http://getcomposer.org/download)

		curl -s http://getcomposer.org/installer | php
5. Run composer in root directory of project (default name is kamo) from commandd line with command "composer install"
6. Navigate to app/config directory and if you don't mind working online only than rename config.server.neon to config.local.neon(Recommended!). Otherwise rename config.offline.neon to config.local.neon.
		If you choosed working offline than you need to open config.local.neon and configure it for communicate with your MySQL server. You also need to download right version of database to your own MySQL server and at each change of server database you need to alter your database too.
7. Open project in your web browser to see if it's working :)